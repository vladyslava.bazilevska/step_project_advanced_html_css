const burgerBtn = document.querySelector('.header__burger');
const mainMenu = document.querySelector('.main-menu');

burgerBtn.addEventListener('click', () => {
    if (burgerBtn.classList.contains('active')) {
        burgerBtn.classList.remove('active');
        mainMenu.classList.remove('active');
    } else {
        burgerBtn.classList.add('active');
        mainMenu.classList.add('active');
    }
});

document.addEventListener('click', (event) => {
    if (!event.target.classList.contains('header__burger')) {
        burgerBtn.classList.remove('active');
        mainMenu.classList.remove('active');
    }
});

